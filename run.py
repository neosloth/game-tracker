from src import bot

token = ""
with open("secret_token") as f:
    token = f.read().strip()

bot = bot.Tracker(token)
bot.run(bot.token)
