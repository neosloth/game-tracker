CREATE TABLE IF NOT EXISTS games(
        date TEXT,

        pilot1 INTEGER,
        pilot2 INTEGER,
        pilot3 INTEGER,
        pilot4 INTEGER,

        deck1 INTEGER,
        deck2 INTEGER,
        deck3 INTEGER,
        deck4 INTEGER,

        winner INTEGER,
        winning_deck INTEGER,

        FOREIGN KEY(pilot1) REFERENCES pilots(id),
        FOREIGN KEY(pilot2) REFERENCES pilots(id),
        FOREIGN KEY(pilot3) REFERENCES pilots(id),
        FOREIGN KEY(pilot4) REFERENCES pilots(id),

        FOREIGN KEY(deck1) REFERENCES decks(id),
        FOREIGN KEY(deck1) REFERENCES decks(id),
        FOREIGN KEY(deck1) REFERENCES decks(id),
        FOREIGN KEY(deck1) REFERENCES decks(id),

        FOREIGN KEY(winner) REFERENCES pilots(id),
        FOREIGN KEY(winning_deck) REFERENCES decks(id)
        );


CREATE TABLE IF NOT EXISTS pilots(
        id INTEGER PRIMARY KEY UNIQUE,
        name TEXT,
        wins INTEGER DEFAULT (0),
        losses INTEGER DEFAULT (0)
        );


CREATE TABLE IF NOT EXISTS decks(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        wins INTEGER DEFAULT (0),
        losses INTEGER DEFAULT (0)
        );
