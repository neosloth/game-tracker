import logging
import logging.handlers

import asyncio
import os
import sqlite3
import discord

from datetime import datetime


class Tracker(discord.Client):

    def __init__(self, token):
        """ Constructor for the  bot class.

        Args:
            token (str): the token for the discord bot.
        """
        super().__init__()

        self.token = token

        self.command = "!record"
        self.command_results = "!statistics"
        self.command_last_winners = "!last_winners"

        self.logger = logging.getLogger('discord')
        self.logger.setLevel(logging.INFO)
        handler = logging.handlers.RotatingFileHandler(filename='discord.log',
                                                       encoding='utf-8',
                                                       mode='w',
                                                       backupCount=1,
                                                       maxBytes=1000000)
        handler.setFormatter(
            logging.Formatter(
                '%(asctime)s:%(levelname)s:%(name)s: %(message)s'))

        self.logger.addHandler(handler)


        self.database = os.path.realpath(os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "../games.db"))


        self.schema = os.path.realpath(os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "../schema.sql"))


    def create_db(self):
        """Creates the database"""
        db = sqlite3.connect(self.database)
        cursor = db.cursor()
        with open(self.schema) as f:
            cursor.executescript(f.read())
        db.commit()
        db.close()

    async def say(self, message, channel):
        """ Wrapper for send_typing and send_message """

        self.logger.info("Saying: {0}".format(message).encode("ascii", "ignore"))
        await self.send_typing(channel)
        await self.send_message(channel, message)

    async def on_message(self, message):
        """Overloaded Method"""
        if message.author != self.user:
            # Get the message itself.
            user = message.author

            # The first word is the command.
            command = message.content.split(" ")[0]
            args = message.content.split(" ")[1::]

            self.logger.info("{0} sent: {1}".format(user, message.content).
                             encode("ascii", "ignore"))

            if (message.server is not None and command == self.command):
                if (len(message.mentions) > len(args[len(message.mentions)::])):
                    await self.say("Please provide a deck for every player.", message.channel)
                    return

                if (not len(message.mentions)):
                    await self.say("Please provide a full list of players in the pod")
                    return

                message.mentions.insert(0, message.author)
                players = []
                decks = []

                # ids of the winners
                winner = 0
                winning_deck = 0
                for i in range(4):
                    try:
                        player = message.mentions[i]
                        # Shift left to avoid the off by one error
                        deck = args[len(message.mentions)-1::][i]

                        self.create_or_update_user(player)
                        deck_id = self.create_or_find_deck(deck)

                        players.append(player.id)
                        decks.append(deck_id)

                        # The person calling the command is the winner
                        if (i == 0):
                            self.add_win_user(player)
                            self.add_win_deck(deck_id)
                            winner = player
                            winning_deck = deck_id

                        else:
                            self.add_loss_user(player)
                            self.add_loss_deck(deck_id)

                    # If we have less than 4 fill the table with null
                    except IndexError:
                        players.append(None)
                        decks.append(None)

                self.add_game(players, decks, winner, winning_deck)

            elif (command == self.command_results):
                await self.say(self.grab_results(message.author.id),
                               message.channel)

            elif (command == self.command_last_winners):
                await self.say(self.last_winners(), message.channel)


    def _sql_wrapper(self, query, values=()):
        db = sqlite3.connect(self.database)
        cursor = db.cursor()
        cursor.execute("PRAGMA foreign_keys = ON;")
        # Is this necessary?
        if (values):
            cursor.execute(query, values)
        else:
            cursor.execute(query)
        result = cursor.fetchall()
        db.commit()
        db.close()
        return result

    def create_or_update_user(self, user):
        self._sql_wrapper('''
                INSERT OR IGNORE into pilots(id, name) VALUES (?,?)
                ''', (user.id, user.name,))


    def add_win_user(self, user):
        self._sql_wrapper('''
        UPDATE pilots SET wins = wins + 1 WHERE id = ?
        ''', (user.id, ))


    def add_win_deck(self, deck):
        self._sql_wrapper('''
        UPDATE decks SET wins = wins + 1 WHERE id = ?
        ''', (deck, ))


    def add_loss_user(self, user):
        self._sql_wrapper('''
        UPDATE pilots SET losses = losses  + 1 WHERE id = ?
        ''', (user.id, ))


    def add_loss_deck(self, deck):
        self._sql_wrapper('''
        UPDATE decks SET losses = losses + 1 WHERE id = ?
        ''', (deck, ))


    def add_game(self, pilots, decks, winner, winning_deck):
        time = datetime.now()

        payload = (time, ) + tuple(pilots) + tuple(decks) + (winner.id,) + (winning_deck,)
        self._sql_wrapper('''
        INSERT into games(date,
        pilot1, pilot2, pilot3, pilot4,
        deck1, deck2, deck3, deck4,
        winner, winning_deck) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        ''', payload)

    def create_or_find_deck(self, deck_name):

        matches = self._sql_wrapper('''
        SELECT id from decks WHERE name LIKE ?
        ''', ('%' + deck_name + '%',))

        if matches:
            deck_name = self._sql_wrapper('''
            SELECT name from decks WHERE id = ?
            ''', (matches[0][0],))[0][0]

        else:
            self._sql_wrapper('''
                INSERT OR IGNORE into decks(name) VALUES (?)
                ''', (deck_name,))
        return self._sql_wrapper('''
        SELECT id from decks WHERE name = ?
        ''', (deck_name,))[0][0]


    def grab_results(self, user_id):
        matches = self._sql_wrapper('''
        SELECT wins, losses FROM pilots WHERE id = ?
        ''', (user_id,))

        if not matches:
            return "No games recorded"

        return "Wins: {0[0]}\nLosses: {0[1]}\n".format(matches[0])


    def last_winners(self):
        matches = self._sql_wrapper('''
        SELECT pilots.name as winner, decks.name, date from pilots,decks,games WHERE winning_deck=decks.id and winner=pilots.id ORDER BY date DESC LIMIT 5;
        ''')

        result = "```"
        for match in matches:
            for column in match:
                if column is not None:
                    result += "{:<10} | ".format(column)
                else:
                    result += "{:<10} | ".format("N/A")
            result += "\n"
        return result + "```"


    async def on_ready(self):
        """Overloaded Method"""
        self.create_db()
        self.logger.info("Logged in as {0}".format(self.user.name))
